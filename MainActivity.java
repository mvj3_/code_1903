case MotionEvent.ACTION_UP:
					//获取按起时的X,Y坐标
					float newX = event.getX();
					float newY = event.getY();
					//通过ListView中的pointToPosition方法获取点击ListView中的位置position
					final int oldPosition = ((ListView)v).pointToPosition((int)oldX, (int)oldY);
					int newPosition = ((ListView)v).pointToPosition((int)newX, (int)newY);
					int FirstVisiblePosition = ((ListView)v).getFirstVisiblePosition();
					
					if( newX - oldX > 20  && oldPosition == newPosition) {
						//获取ListView中点击是的View
						view = ((ListView)v).getChildAt(oldPosition);
						 if (view == null) {                    
			                              view = ((ListView) v).getChildAt(newPosition - FirstVisiblePosition);  
						 }  
						for(int n=0;n<data.size();n++)
						{
							if(n==oldPosition)
							{
								dataMap.put(n, true);
							}
							else
							{
								dataMap.put(n, false);
							}
						}
						adapter.notifyDataSetChanged();
					}
					break;